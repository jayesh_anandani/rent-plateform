## Installation

```bash
$ npm install
```

## Running the app

```bash
$ npm run start

```
## Instruction
Copy .env.example to .env file.