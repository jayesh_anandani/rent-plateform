import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { MongooseModule } from '@nestjs/mongoose';
import { UserModule } from './modules/user/user.module';
import { ProductModule } from './modules/product/product.module';
import { APP_GUARD } from '@nestjs/core';
import { RolesGuard } from './decorator/role.guard';
import { userSchema } from './modules/user/schema/user.schema';
import { RentProductModule } from './modules/rent-product/rent-product.module';
import { ConfigModule, ConfigService } from 'nestjs-config';
import * as path from 'path';
@Module({
  imports: [
    GraphQLModule.forRoot({
      autoSchemaFile: 'rent.gql',
      playground: true,
    }),
    ConfigModule.load(path.resolve(__dirname, '../src/config/**/*.{ts,js}')),
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (config: ConfigService) => ({
        uri: config.get('database.dbUrl'),
      }),
      inject: [ConfigService],
    }),
    MongooseModule.forFeature([{ name: 'RENT_USER', schema: userSchema }]),
    UserModule,
    RentProductModule,
    ProductModule,
  ],
  providers: [
    {
      provide: APP_GUARD,
      useClass: RolesGuard,
    },
  ],
})
export class AppModule {}
