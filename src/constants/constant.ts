export const jwtConstants = {
  secret: 'rentUser',
  expiresIn: '1d',
};

export enum MODEL_NAME {
  USER = 'RENT_USER',
  PRODUCT = 'PRODUCT',
  RENT_PRODUCT = 'RENT_PRODUCT',
}

export const staticError = {
  alreayRent: 'already on rent',
  userExist: 'User already exist',
  userNotFound: 'User not found',
  assinedRentProduct: 'assigned rent-product not allowed to',
  passwordNotMatched: 'Password Not Matched',
  user:'USER_LOGIN',
  rent:'RENT',
  rentProduct:'RENT_PRODUCT'
};