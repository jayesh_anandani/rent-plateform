import { Field, InputType, OmitType } from '@nestjs/graphql';

@InputType()
export class ProductInput {
  @Field(() => String, { nullable: false })
  name: string;

  @Field(() => Number, { nullable: false })
  price: number;

  @Field(() => String, { nullable: false })
  user: string;

  @Field(() => Boolean, { defaultValue: false })
  rented: boolean;

  @Field(() => Number, { nullable: false })
  costPrice: number;
}

@InputType()
export class ProductUpdateInput extends OmitType(ProductInput, [] as const) {
  @Field(() => String)
  _id: string;
}
