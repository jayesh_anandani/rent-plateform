import { ProductResolver } from './resolver/product.resolver';
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import {  productSchema } from './schema/product.schema';
import { ProductService } from './product.service';
import { UserService } from 'src/modules/user/user.service';
import { userSchema } from 'src/modules/user/schema/user.schema';
import { MODEL_NAME } from 'src/constants/constant';
import { rentProductSchema } from '../rent-product/schema/rent-product.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: MODEL_NAME.PRODUCT, schema: productSchema },
      { name: MODEL_NAME.USER, schema: userSchema },
      { name: MODEL_NAME.RENT_PRODUCT, schema: rentProductSchema },
    ]),
  ],
  controllers: [],
  providers: [ProductResolver, ProductService, UserService],
})
export class ProductModule {}
