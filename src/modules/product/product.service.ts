import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { ObjectId } from 'mongodb';
import { IProduct } from './interface/product.interface';

@Injectable()
export class ProductService {
  constructor(
    @InjectModel('PRODUCT')
    private productModel: Model<IProduct>,
  ) {}

  /**
   * fetch all productList
   * @param query
   */
  async findAll() {
    return await this.productModel.aggregate();
  }

  /**
   * fetch all productList
   * @param query
   */
  async getMany(query) {
    return await this.productModel.aggregate();
  }

  /**
   * single product
   * @param query
   */
  async findOne(query): Promise<IProduct> {
    return await this.productModel.findOne(query).lean();
  }

  /**
   *
   * @param payload
   */
  async createMany(payload): Promise<any> {
    await this.productModel.deleteMany({
      _id: { $in: payload.map(doc => new ObjectId(doc._id)) },
    });
    return await this.productModel.create(payload);
  }

  /**
   * register product
   * @param payload
   */
  async create(payload): Promise<IProduct> {
    return await this.productModel.create(payload);
  }

  /**
   * update product
   * @param payload
   */
  async update(payload): Promise<IProduct> {
    return await this.productModel.findOneAndUpdate(
      {
        _id: new ObjectId(payload._id),
      },
      payload,
      {
        new: true,
        upsert: true,
      },
    );
  }

  /**
   * delete product
   * @param id
   */
  async remove(id): Promise<IProduct> {
    return await this.productModel.findOneAndDelete({ _id: new ObjectId(id) });
  }
}
