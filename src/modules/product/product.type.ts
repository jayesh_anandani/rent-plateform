import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType({ isAbstract: true })
export class Product {
  @Field(() => String, { nullable: false })
  _id: string;

  @Field(() => String, { nullable: false })
  name: string;

  @Field(() => Number, { nullable: false })
  price: number;

  @Field(() => Number, { nullable: false })
  costPrice: number;
}
