import { Resolver, Mutation, Args } from '@nestjs/graphql';

import { Query } from '@nestjs/graphql';
import { ObjectId } from 'mongodb';
import { ProductService } from '../product.service';
import { ProductInput, ProductUpdateInput } from '../input/product.input';
import { Product } from '../product.type';
import { CurrentUser } from 'src/decorator/user.decorator';
import { IUser } from 'src/modules/user/interface/user.interface';
import { UseGuards, BadRequestException } from '@nestjs/common';
import { GqlAuthGuard } from 'src/decorator/auth-guard';
import { UserService } from 'src/modules/user/user.service';
import { staticError } from 'src/constants/constant';

@Resolver(of => Product)
export class ProductResolver {
  constructor(
    private readonly productService: ProductService,
    private readonly userService: UserService,
  ) {}
  @Query(() => String, { name: 'getOneProduct' })
  async getOneProduct() {
    return 'aneri';
  }

  @UseGuards(GqlAuthGuard)
  @Mutation(() => Product, { name: 'createProduct' })
  async create(@Args('input') input: ProductInput, @CurrentUser() user: IUser) {
    let payload = { ...input };
    return this.productService.create({
      ...payload,
      userId: new ObjectId(user._id),
    });
  }

  @UseGuards(GqlAuthGuard)
  @Mutation(() => Product, { name: 'updateProduct' })
  async update(@Args('input') input: ProductUpdateInput) {
    try {
      let payload = { ...input };

      const assignProducts = await this.productService.findOne({
        _id: new ObjectId(input._id),
      });
      if (assignProducts.rented)
        throw new Error(`${staticError.assinedRentProduct} update`);
      const user = await this.productService.update(payload);
      return user;
    } catch (error) {
      throw new BadRequestException(
        (error as Error).message,
        'RENT_UPDATE.ERROR',
      );
    }
  }

  @Mutation(() => Product, { name: 'deleteProduct' })
  async remove(@Args('id') productId: string) {
    try {
      const assignProducts = await this.productService.findOne({
        _id: new ObjectId(productId),
      });
      if (assignProducts.rented)
        throw new Error(`${staticError.assinedRentProduct} delete`);
      return await this.productService.remove(productId);
    } catch (error) {
      throw new BadRequestException(
        (error as Error).message,
        'RENT_DELETE.ERROR',
      );
    }
  }
}
