import * as mongoose from 'mongoose';

export const productSchema = new mongoose.Schema(
  {
    name: {
      type: mongoose.Schema.Types.String,
      required: true,
    },
    price: {
      type: mongoose.Schema.Types.Number,
      required: true,
    },
    rented: {
      type: mongoose.Schema.Types.Number,
      default: false,
    },
    costPrice: {
      type: mongoose.Schema.Types.Number,
      required: true,
    },
  },
  { timestamps: true },
);
