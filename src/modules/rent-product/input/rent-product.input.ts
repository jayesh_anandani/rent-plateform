import { Field, InputType, OmitType } from '@nestjs/graphql';

@InputType()
export class RentProductInput {
  @Field(() => String, { nullable: false })
  productId: string;
  @Field(() => String, { nullable: false })
  userId: string;
}

@InputType()
export class RentProductUpdateInput extends OmitType(
  RentProductInput,
  [] as const,
) {
  @Field(() => String)
  _id: string;
}



@InputType()
export class UserRentInput {
  @Field(() => String)
  productId: string;

  @Field(() => String, { nullable: true })
  startDate: string;

  @Field(() => String, { nullable: true })
  endDate: string;
}
