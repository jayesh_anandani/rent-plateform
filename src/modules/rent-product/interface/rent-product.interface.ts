import { Document, ObjectId } from 'mongoose';
export interface IRentProduct extends Document {
  userId:ObjectId
  productId:ObjectId
  createdAt?: Date;
  updatedAt?: Date;
  startDate?:Date;
  endDate?:Date;

}
