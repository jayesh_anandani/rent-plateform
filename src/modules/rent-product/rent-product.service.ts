import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { ObjectId } from 'mongodb';
import { IRentProduct } from './interface/rent-product.interface';

@Injectable()
export class RentProductService {
  constructor(
    @InjectModel('RENT_PRODUCT')
    private rentProductModel: Model<IRentProduct>,
  ) {}

  /**
   * fetch all rentProductList
   * @param query
   */
  async findAll() {
    return await this.rentProductModel.aggregate();
  }

  /**
   * fetch all rentProductList
   * @param query
   */
  async getMany(query) {
    return await this.rentProductModel.aggregate();
  }

  /**
   * single rentProduct
   * @param query
   */
  async findOne(query): Promise<IRentProduct> {
    return await this.rentProductModel.findOne(query).lean();
  }

  /**
   *
   * @param payload
   */
  async createMany(payload): Promise<any> {
    await this.rentProductModel.deleteMany({
      _id: { $in: payload.map(doc => new ObjectId(doc._id)) },
    });
    return await this.rentProductModel.create(payload);
  }

  /**
   * register rentProduct
   * @param payload
   */
  async create(payload): Promise<IRentProduct> {
    return await this.rentProductModel.create(payload);
  }

  /**
   * update rentProduct
   * @param payload
   */
  async update(payload): Promise<IRentProduct> {
    return await this.rentProductModel.findOneAndUpdate(
      {
        _id: new ObjectId(payload._id),
      },
      payload,
      {
        new: true,
        upsert: true,
      },
    );
  }

  /**
   * delete rentProduct
   * @param id
   */
  async remove(id): Promise<IRentProduct> {
    return await this.rentProductModel.findOneAndDelete({ _id: new ObjectId(id) });
  }
}
