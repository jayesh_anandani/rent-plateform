import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType({ isAbstract: true })
export class RentProduct {
  @Field(() => String, { nullable: false })
  _id: string;

  @Field(() => String, { nullable: false })
  productId: string;

  @Field(() => String, { nullable: false })
  startDate: string;

  @Field(() => String, { nullable: false })
  endDate: string;
}
