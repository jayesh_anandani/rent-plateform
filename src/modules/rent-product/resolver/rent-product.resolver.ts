import { Resolver, Mutation, Args } from '@nestjs/graphql';

import { Query } from '@nestjs/graphql';
import { ObjectId } from 'mongodb';
import {
  RentProductInput,
  RentProductUpdateInput,
} from '../input/rent-product.input';
import { RentProduct } from '../rent-product.type';
import { CurrentUser } from 'src/decorator/user.decorator';
import { IUser } from 'src/modules/user/interface/user.interface';
import { UseGuards, BadRequestException } from '@nestjs/common';
import { GqlAuthGuard } from 'src/decorator/auth-guard';
import { UserService } from 'src/modules/user/user.service';
import { RentProductService } from '../rent-product.service';
import { staticError } from 'src/constants/constant';
import { User } from 'src/modules/user/user.type';
import { UserRentInput } from 'src/modules/rent-product/input/rent-product.input';

@Resolver(of => RentProduct)
export class RentProductResolver {
  constructor(
    private readonly rentService: RentProductService,
    private readonly userService: UserService,
    private readonly rentProductService: RentProductService,
  ) {}

  @UseGuards(GqlAuthGuard)
  @Mutation(() => RentProduct, { name: 'createRentProduct' })
  async create(
    @Args('input') input: RentProductInput,
    @CurrentUser() user: IUser,
  ) {
    let payload = { ...input };
    return this.rentService.create({
      ...payload,
      userId: new ObjectId(user._id),
    });
  }

  @UseGuards(GqlAuthGuard)
  @Mutation(() => RentProduct, { name: 'updateRentProduct' })
  async update(@Args('input') input: RentProductUpdateInput) {
    try {
      let payload = { ...input };

      const assignRents = await this.userService.findOne({
        productIds: { $in: [payload._id] },
      });
      if (assignRents)
        throw new Error(`${staticError.assinedRentProduct} update`);
      const user = await this.rentService.update(payload);
      return user;
    } catch (error) {
      throw new BadRequestException(
        (error as Error).message,
        `${staticError.rentProduct}update error`,
      );
    }
  }

  @Mutation(() => RentProduct, { name: 'deleteRentProduct' })
  async remove(@Args('id') productId: string) {
    try {
      const assignRents = await this.userService.findOne({
        productIds: { $in: [productId] },
      });
      if (assignRents)
        throw new Error(`${staticError.assinedRentProduct} delete`);
      return await this.rentService.remove(productId);
    } catch (error) {
      throw new BadRequestException(
        (error as Error).message,
        `${staticError.rentProduct}delete error`,
      );
    }
  }

  @Mutation(() => User, { name: 'addUserRent' })
  async addUserRent(@Args('input') input: UserRentInput, @CurrentUser() req) {
    const assignRents = await this.rentProductService.findOne({
      _id: new ObjectId(input.productId),
    });
    if (assignRents.productId) throw new Error(`${staticError.alreayRent}`);
    return this.rentProductService.create({
      ...input,
      userId: new ObjectId(req._id),
    });
  }
}
