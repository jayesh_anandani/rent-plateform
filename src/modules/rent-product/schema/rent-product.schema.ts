import * as mongoose from 'mongoose';

export const rentProductSchema = new mongoose.Schema(
  {
    productId: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
    },
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
    },
    startDate: {
      type: mongoose.Schema.Types.String,
      required: false,
    },
    endDate: {
      type: mongoose.Schema.Types.String,
      required: false,
    },
  },
  { timestamps: true },
);
