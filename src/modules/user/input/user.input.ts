import { Field, InputType, OmitType } from '@nestjs/graphql';

@InputType()
export class UserInput {
  @Field(() => String, { nullable: false })
  name: string;

  @Field(() => String, { nullable: false })
  email: string;

  @Field(() => Number, { nullable: false })
  phoneNumber: number;

  @Field(() => String)
  address: string;

  @Field(() => String, { nullable: false })
  password: string;
}

@InputType()
export class UserUpdateInput extends OmitType(UserInput, ['email'] as const) {
  @Field(() => String)
  _id: string;
}
