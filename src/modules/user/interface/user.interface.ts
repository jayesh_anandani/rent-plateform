import { Document } from 'mongoose';
export interface IUser extends Document {
  name: string;
  email: string;
  phoneNumber: number;
  address: string;
  password: string;
  createdAt?: Date;
  updatedAt?: Date;
}
