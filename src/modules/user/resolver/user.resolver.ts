import { Resolver, Args } from '@nestjs/graphql';
import { BadRequestException } from '@nestjs/common';
import { Mutation } from '@nestjs/graphql';
import { ObjectId } from 'mongodb';

import { User } from '../user.type';
import { LoginResponse } from 'src/response/login.response';
import { LoginInput } from '../input/login.input';
import { UserService } from '../user.service';
import { UserInput, UserUpdateInput } from '../input/user.input';
import { staticError } from 'src/constants/constant';
import { RentProductService } from 'src/modules/rent-product/rent-product.service';

@Resolver(of => User)
export class UserResolver {
  constructor(
    private readonly userService: UserService,
    private readonly rentProductService: RentProductService,
  ) {}

  @Mutation(() => User, { name: 'createUser' })
  async create(@Args('input') input: UserInput) {
    let payload = { ...input };
    payload.email = payload.email.toLowerCase();
    const existUser = await this.userService.findOne({
      email: payload.email,
    });
    console.log({ existUser });
    if (existUser) throw new Error(`${staticError.userExist}`);
    return this.userService.create({
      ...payload,
    });
  }

  @Mutation(() => User, { name: 'updateUser' })
  async update(@Args('input') input: UserUpdateInput) {
    try {
      let payload = { ...input };
      const user = await this.userService.update(payload);
      return user;
    } catch (error) {
      throw new BadRequestException(
        (error as Error).message,
        `${staticError.user}_UPDATE.ERROR`,
      );
    }
  }

  @Mutation(() => LoginResponse, { name: 'login' })
  async login(@Args('input') input: LoginInput) {
    try {
      input.email = input.email.toLowerCase();
      return await this.userService.login(input);
    } catch (error) {
      throw new BadRequestException(
        (error as Error).message,
        `${staticError.user}_LOGIN.ERROR`,
      );
    }
  }

}
