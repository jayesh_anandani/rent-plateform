import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ProductModule } from 'src/modules/product/product.module';
import { MODEL_NAME } from 'src/constants/constant';
import { userSchema } from 'src/modules/user/schema/user.schema';
import { UserService } from 'src/modules/user/user.service';
import { UserResolver } from 'src/modules/user/resolver/user.resolver';
import { rentProductSchema } from '../rent-product/schema/rent-product.schema';
import { productSchema } from '../product/schema/product.schema';
import { RentProductService } from '../rent-product/rent-product.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: MODEL_NAME.USER, schema: userSchema },
      { name: MODEL_NAME.RENT_PRODUCT, schema: rentProductSchema },
      { name: MODEL_NAME.PRODUCT, schema: productSchema },
    ]),
    ProductModule,
  ],
  controllers: [],
  providers: [UserResolver, UserService, RentProductService],
})
export class UserModule {}
