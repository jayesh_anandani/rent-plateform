import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { JwtService } from '@nestjs/jwt';
import { ObjectId } from 'mongodb';
import Cryptr from 'cryptr';
import * as bcrypt from 'bcrypt';

import { IUser } from './interface/user.interface';
import { LoginInput } from './input/login.input';
import { jwtConstants, staticError } from 'src/constants/constant';

@Injectable()
export class UserService {
  constructor(
    @InjectModel('RENT_USER')
    private userModel: Model<IUser>,
  ) {}

  /**
   * fetch all users
   * @param query
   */
  async findAll() {
    return await this.userModel.aggregate();
  }

  /**
   * fetch all users
   * @param query
   */
  async getMany(query) {
    return await this.userModel.aggregate();
  }

  /**
   * single user
   * @param query
   */
  async findOne(query): Promise<IUser> {
    console.log({ query });
    return await this.userModel.findOne(query).lean();
  }

  /**
   *
   * @param payload
   */
  async createMany(payload): Promise<any> {
    await this.userModel.deleteMany({
      _id: { $in: payload.map(doc => new ObjectId(doc._id)) },
    });
    return await this.userModel.create(payload);
  }

  /**
   * register user
   * @param payload
   */
  async create(payload): Promise<IUser> {
    const hashedPassword = await bcrypt.hash(payload.password, 12);
    payload.password = hashedPassword;
    return await this.userModel.create(payload);
  }

  /**
   * update user
   * @param payload
   */
  async update(payload): Promise<IUser> {
    return await this.userModel.findOneAndUpdate(
      {
        _id: new ObjectId(payload._id),
      },
      ...payload,
      {
        new: true,
        upsert: true,
      },
    );
  }

  /**
   * delete user
   * @param id
   */
  async remove(id): Promise<IUser> {
    return await this.userModel.findOneAndDelete({ _id: new ObjectId(id) });
  }

  /**
   * login user
   * @param id
   */
  async login(loginInfo: LoginInput) {
    const jwt = new JwtService({
      secret: jwtConstants.secret,
      signOptions: { expiresIn: jwtConstants.expiresIn },
    });
    const existUser = await this.userModel.findOne({
      email: loginInfo.email,
    });

    if (!existUser) {
      throw new Error(staticError.userNotFound);
    }

    let result = null;

    result = await bcrypt.compare(loginInfo.password, existUser.password);

    if (result) {
      const payload = {
        userId: new ObjectId(existUser._id),
      };
      console.log({ payload });
      const token = jwt.sign(payload);
      return { token, userInfo: existUser };
    }
    throw new Error(staticError.passwordNotMatched);
  }
}
