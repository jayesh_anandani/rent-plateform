import { Field, ObjectType } from '@nestjs/graphql';
import { Product } from 'src/modules/product/product.type';

@ObjectType({ isAbstract: true })
export class User {
  @Field(() => String, { nullable: false })
  _id: string;

  @Field(() => String, { nullable: false })
  name: string;

  @Field(() => String, { nullable: false })
  email: string;

  @Field(() => Number, { nullable: false })
  phoneNumber: number;

  @Field(() => String)
  address: string;

  @Field(() => String, { nullable: false })
  password: string;

}
