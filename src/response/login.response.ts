import { ObjectType, Field } from '@nestjs/graphql';
import { User } from 'src/modules/user/user.type';

@ObjectType({ isAbstract: true })
export class LoginResponse {
  @Field(() => String, { nullable: false })
  public token: string;

  @Field(() => User, { nullable: true })
  public userInfo: User;
}
